# JavaSCRIPT <!-- .element: style="color: #f7df1e" -->
##### ISEN Novembre Décembre 2018
com.acordier@gmail.com <!-- .element: class="fragment grow" -->


---

## Organisation

- 14 séances <!-- .element: class="fragment grow" -->
  - 14 h de cours <!-- .element: class="fragment" -->
  - 14 h de tp <!-- .element: class="fragment" -->


---

## Programme

- [Rappels] API DOM - JS <!-- .element: class="fragment" -->
- [Rappels] événements du DOM <!-- .element: class="fragment" -->
- Notion de prototype <!-- .element: class="fragment" -->
- Héritage prototypal <!-- .element: class="fragment" -->
- Gestion des dépendances et modules  <!-- .element: class="fragment" -->
- Templating et composants <!-- .element: class="fragment" -->
- Pour aller plus loin <!-- .element: class="fragment" -->

Note:
- Anything after `Note:` will only appear here

---

# Questions ?  


---

## [API] DOM <!-- .element: style="position: absolute; left: 50%; top: 50%" -->

![tree](/images/tree.svg) <!-- .element: style="background-color: transparent; border: none; width: 500px; box-shadow: none; margin-top: -100px" -->



---

### Browser architecture overview

#### User interface <!-- .element: style="background-color: #f7df1e; color: #222;  width: 350px; margin: 5px auto; font-weight:1200;" class="fragment" data-fragment-index="1"-->
#### Browser engine <!-- .element: style="background-color: #f7df1e; color: #222;  width: 350px; margin: 5px auto; font-weight:1200;" class="fragment" data-fragment-index="2"-->
#### Render engine <!-- .element: style="background-color: #f7df1e; color: #222;  width: 350px; height:100px; margin: 5px auto; display: flex; flex-direction: column; align-items: center; justify-content: center" class="fragment" data-fragment-index="3" -->
#### Js Engine <!-- .element: style="background-color: #f7df1e; color: #222; width: 270px; margin: 5px auto;display: inline-block" class="fragment" data-fragment-index="5"-->
#### XML parser <!-- .element: style="background-color: #f7df1e; color: #222; width: 250px; margin: 5px auto;display: inline-block" class="fragment" data-fragment-index="4" -->
#### WASM engine <!-- .element: style="background-color: #f7df1e; color: #222; width: 270px; margin: 5px auto;display: inline-block"   class="fragment" data-fragment-index="5" -->
#### Network <!-- .element: style="background-color: #f7df1e; color: #222; width: 300px; margin: 5px auto;display: inline-block" class="fragment" data-fragment-index="6"-->
#### TLS <!-- .element: style="background-color: #f7df1e; color: #222; width: 300px; margin: 5px auto;display: inline-block" class="fragment" data-fragment-index="6"-->
#### DATA PERSISTENCE <!-- .element: style="background-color: #f7df1e; color: #222; display: block; width: 300px; margin: 5px auto;" class="fragment" data-fragment-index="7"-->


---

## Render no matter what

- DOM is rendered as soon as possible <!-- .element: class="fragment" -->
- load event occurs at the end by contract <!-- .element: class="fragment" -->
- HTML parser is (really) fault tolerant <!-- .element: class="fragment" -->

---

### Webkit source code excerpts


---
```
Support for really broken HTML. We never close the body tag, 
since some stupid web pages close it before the actual end 
of the doc. Let's rely on the end() call to close things.
```
---
```
www.liceo.edu.mx is an example of a site that achieves 
a level of nesting of about 1500 tags, all from a bunch 
of <b>s. We will only allow at most 20 nested tags of 
the same type before just ignoring them all together.
```

---
## Render Flow 
#### From content to pixels

![renderFlow](/images/rendering.svg) <!-- .element: style="background-color: transparent; border: none; width: 90%; box-shadow: none;" -->


---
## Render Object

```
class RenderObject{
  virtual void layout();
  virtual void paint(PaintInfo);
  virtual void rect repaintRect();
  Node* node;  //the DOM node
  RenderStyle* style;  // the computed style
  RenderLayer* containgLayer; //the containing z-index layer
}
```

---


## The DOM API
> “A platform- and language-neutral interface that will allow programs
and scripts to dynamically access and update the content, structure, and style of documents.”  <!-- .element: style="padding: 15px; text-align: center;" -->

---

## DOM TREE

![renderFlow](/images/render-dom.svg) <!-- .element: style="background-color: transparent; border: none; width: 70%; box-shadow: none; margin-top: -20px"" -->


---

## HTML DOM is reentrant

> Any modification on a visible DOM node will trigger a render reflow <!-- .element: style="padding: 15px; text-align: center;" -->


---
# Dom levels


---
## Level 1
```
interface Document : Node {
  Element createElement(DOMString);
  
  DocumentFragment createDocumentFragment();
  
  Text createTextNode(DOMString);
  
  Attr createAttribute(DOMString);
  
  NodeList getElementsByTagName(DOMString);
  
  // ...
};

```

---
## Level 2

```
interface EventTarget {
  void addEventListener(DOMString, EventListen, boolean);
  
  void removeEventListener(DOMString, EventListen, boolean);
  
  boolean dispatchEvent(Event);
};

```

---
## Level 3
```
interface KeyboardEvent : UIEvent {
  readonly attribute DOMString key;
  
  readonly attribute DOMString code;

  readonly attribute boolean ctrlKey;
  
  readonly attribute boolean shiftKey;
  
  readonly attribute boolean altKey;

};
```


---
## Selectors API

```
partial interface Document {
  Element?  querySelector(DOMString selectors);
  NodeList  querySelectorAll(DOMString selectors);
};

partial interface DocumentFragment {
  Element?  querySelector(DOMString selectors);
  NodeList  querySelectorAll(DOMString selectors);
};

partial interface Element {
  Element?  querySelector(DOMString selectors);
  NodeList  querySelectorAll(DOMString selectors);
};
```

---

